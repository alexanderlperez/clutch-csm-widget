<?php 

add_action( 'widgets_init', 'register_sm_widget' );
function register_sm_widget () { register_widget( 'CSM_Widget' ); }

class CSM_Widget extends WP_Widget { 
	function __construct() {
		parent::WP_Widget( 'csm_widget', 
			__( 'CSM Widget', 'csm-widget' ), 
			array( 'description' => __( 'Social Media Buttons with Twitter Feed', 'csm-widget') )
		);
	}

	function widget( $args, $instance ) {
		// external
		include_once( 'csm-markup.php' );
	}
	
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance[ 'user' ] 		= $new_instance[ 'user' ];
		$instance[ 'api_key' ] 		= $new_instance[ 'api_key' ];
		$instance[ 'num_tweets' ] 	= $new_instance[ 'num_tweets' ];
		$instance[ 'height' ] 		= $new_instance[ 'height' ];
		$instance[ 'facebook-page' ] 	= $new_instance[ 'facebook-page' ];
		$instance[ 'twitter-page' ] 	= $new_instance[ 'twitter-page' ];
		$instance[ 'googleplus-page' ]  = $new_instance[ 'googleplus-page' ];
		$instance[ 'pinterest-page' ]   = $new_instance[ 'pinterest-page' ];
		$instance[ 'tumblr-page' ] 	= $new_instance[ 'tumblr-page' ];
		return $instance;
	}
	
	function form( $instance ) {
		// check if the fields have values already
		if ( $instance ) {
			$user 		= esc_attr( $instance[ 'user' ] );
			$apikey 	= esc_attr( $instance[ 'api_key' ] );
			$numtweets 	= esc_attr( $instance[ 'num_tweets' ] );
			$height 	= esc_attr( $instance[ 'height' ] );
			$facebookpage 	= esc_attr( $instance[ 'facebook-page' ] ); 
			$twitterpage 	= esc_attr( $instance[ 'twitter-page' ] ); 
			$googlepluspage = esc_attr( $instance[ 'googleplus-page' ] ); 
			$pinterestpage  = esc_attr( $instance[ 'pinterest-page' ] ); 
			$tumblrpage 	= esc_attr( $instance[ 'tumblr-page' ] ); 
		} else {
			$user = '';
			$apikey = '';
			$numtweets = '';
			$height = '';
			$facebookpage = '';
			$twitterpage = '';
			$googlepluspage = '';
			$pinterestpage = '';
			$tumblrpage = '';
		}

		?>
			<p>
				<label for="<?php echo $this->get_field_id( 'user' ); ?>">Twitter user</label>
				<input class="widefat" id="<?php echo $this->get_field_id( 'user' ); ?>" name="<?php echo $this->get_field_name( 'user' ); ?>" type="text" value="<?php echo $user; ?>">
				</input>
			</p>
			<p>
				<label for="<?php echo $this->get_field_id( 'api_key' ); ?>">Twitter API key</label>
				<input class="widefat" id="<?php echo $this->get_field_id( 'api_key' ); ?>" name="<?php echo $this->get_field_name( 'api_key' ); ?>" type="text" value="<?php echo $apikey; ?>">
				</input>
			</p>
			<p>
				<label for="<?php echo $this->get_field_id( 'num_tweets' ); ?>">Number of Tweets to show</label>
				<input class="widefat" id="<?php echo $this->get_field_id( 'num_tweets' ); ?>" name="<?php echo $this->get_field_name( 'num_tweets' ); ?>" type="text" value="<?php echo $numtweets; ?>">
				</input>
			</p>
			<p>
				<label for="<?php echo $this->get_field_id( 'height' ); ?>">Height</label>
				<input class="widefat" id="<?php echo $this->get_field_id( 'height' ); ?>" name="<?php echo $this->get_field_name( 'height' ); ?>" type="text" value="<?php echo $height; ?>">
				</input>
			</p>
			<?php // page links ?>
			<p>
				<label for="<?php echo $this->get_field_id( 'facebook-page' ); ?>">Facebook Page Link</label>
				<input class="widefat" id="<?php echo $this->get_field_id( 'facebook-page' ); ?>" name="<?php echo $this->get_field_name( 'facebook-page' ); ?>" type="text" value="<?php echo $facebookpage; ?>">
				</input>
			</p>
			<p>
				<label for="<?php echo $this->get_field_id( 'twitter-page' ); ?>">Twitter Page Link</label>
				<input class="widefat" id="<?php echo $this->get_field_id( 'twitter-page' ); ?>" name="<?php echo $this->get_field_name( 'twitter-page' ); ?>" type="text" value="<?php echo $twitterpage; ?>">
				</input>
			</p>
			<p>
				<label for="<?php echo $this->get_field_id( 'googleplus-page' ); ?>">Google+  Page</label>
				<input class="widefat" id="<?php echo $this->get_field_id( 'googleplus-page' ); ?>" name="<?php echo $this->get_field_name( 'googleplus-page' ); ?>" type="text" value="<?php echo $googlepluspage; ?>">
				</input>
			</p>
			<p>
				<label for="<?php echo $this->get_field_id( 'pinterest-page' ); ?>">Pinterest Page</label>
				<input class="widefat" id="<?php echo $this->get_field_id( 'pinterest-page' ); ?>" name="<?php echo $this->get_field_name( 'pinterest-page' ); ?>" type="text" value="<?php echo $pinterestpage; ?>">
				</input>
			</p>
			<p>
				<label for="<?php echo $this->get_field_id( 'tumblr-page' ); ?>">Tumblr Page</label>
				<input class="widefat" id="<?php echo $this->get_field_id( 'tumblr-page' ); ?>" name="<?php echo $this->get_field_name( 'tumblr-page' ); ?>" type="text" value="<?php echo $tumblrpage; ?>">
				</input>
			</p>
			<?php // end page links ?>
		<?php 

	}
}
?>
