<?php
/**
 * Plugin Name: Clutch Social Media Pages and Twitter Feed
 * Plugin URI: 
 * Description: Shows the social media pages for the site as well as the site's twitter feed
 * Version: 1.0
 * Author: Alexander Perez
 * Author URI: http://alexanderlperez.com
 * Text Domain: csm-widget
 * License: none
 */

include_once( 'sm_buttons_and_twitter_feed.php' );

?>
