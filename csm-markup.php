<?php /* <link href="<?php echo plugins_url( 'csm-styles.css', __FILE__ ); ?>" rel="stylesheet" type="text/css"> */ ?>
<h3 class="csm-header sidebar-widget-header">Socialize @Clutch</h3>
<div class="csm-wrapper">
	<div class="sm-icon-wrapper facebook">
		<a class="sm-icon-link" href="<?php echo $instance[ 'facebook-page' ]; ?>">
			<img src="<?php echo plugins_url( 'imgs/facebook.png', __FILE__ ); ?>">
		</a>
	</div>
	<div class="sm-icon-wrapper twitter">
		<a class="sm-icon-link" href="<?php echo $instance[ 'twitter-page' ]; ?>">
			<img src="<?php echo plugins_url( 'imgs/twitter.png', __FILE__ ); ?>">
		</a>
	</div>
	<div class="sm-icon-wrapper googleplus">
		<a class="sm-icon-link" href="<?php echo $instance[ 'googleplus-page' ]; ?>">
			<img src="<?php echo plugins_url( 'imgs/googleplus.png', __FILE__ ); ?>">
		</a>
	</div>
	<div class="sm-icon-wrapper pinterest">
		<a class="sm-icon-link" href="<?php echo $instance[ 'pinterest-page' ]; ?>">
			<img src="<?php echo plugins_url( 'imgs/pinterest.png', __FILE__ ); ?>">
		</a>
	</div>
	<div class="sm-icon-wrapper tumblr">
		<a class="sm-icon-link" href="<?php echo $instance[ 'tumblr-page' ]; ?>">
			<img src="<?php echo plugins_url( 'imgs/tumblr.png', __FILE__ ); ?>">
		</a>
	</div>
</div>
<div class="twitter-feed" >
	<a class="twitter-timeline" href="https://twitter.com/<?php echo $instance[ 'user' ]; ?>" height="<?php echo $instance[ 'height' ]; ?>" data-tweet-limit="<?php echo $instance[ 'num_tweets' ]; ?>" data-widget-id="<?php echo $instance[ 'api_key' ]; ?>" data-chrome="noheader nofooter noscrollbar"></a>
	<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
</div>
